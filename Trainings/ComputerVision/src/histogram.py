from matplotlib.widgets import RectangleSelector
import numpy as np
import matplotlib.pyplot as plt
import cv2
import cv2.cv as cv
import skimage.exposure as exposure

x1 = 0
y1 = 0
x2 = 0
y2 = 0

def line_select_callback(eclick, erelease):
    'eclick and erelease are the press and release events'
    global x1,y1,x2,y2
    x1, y1 = eclick.xdata, eclick.ydata
    x2, y2 = erelease.xdata, erelease.ydata
    #print ("(%3.2f, %3.2f) --> (%3.2f, %3.2f)" % (x1, y1, x2, y2))
    #print (" The button you used were: %s %s" % (eclick.button, erelease.button))

# convert image to more suitable formats
image = cv2.imread("sky.jpg")
image_rgb = cv2.cvtColor(image, cv.CV_BGR2RGB)
image_hsv = cv2.cvtColor(image,cv.CV_BGR2HSV)

# select hue
hue = image_hsv[:,:,0]

ax_image = plt.imshow(image_rgb)

# drawtype is 'box' or 'line' or 'none'
RS = RectangleSelector(ax_image.axes, line_select_callback,
                                       drawtype='box', useblit=True,
                                       button=[1,3], # don't use middle button
                                       minspanx=5, minspany=5,
                                       spancoords='pixels')
plt.show()

selected_patch_hue = hue[y1:y2,x1:x2]
hist,bin_edges = np.histogram(selected_patch_hue,bins=256,range=(0,256),density=True)
print hist
print bin_edges

plt.figure()
ax = plt.gca()
ax.bar(bin_edges[:-1],hist)
ax.set_xticks(bin_edges[:-1]+0.5)

back_projection = hist[hue] #cv2.calcBackProject([hue], (0), hist, ranges=[(0,256)],scale=1,uniform=True)
threshold = 0.1
conn_sky = back_projection >= threshold

sky_image = image_rgb.copy()
sky_image[~conn_sky] = [0,0,0]

plt.figure()
plt.imshow(image_rgb)

plt.figure()
plt.imshow(back_projection)
plt.colorbar()

plt.figure()
plt.imshow(sky_image)

plt.show()


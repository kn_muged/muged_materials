import numpy as np
import numpy.random as rand
import numpy.linalg as linalg
import matplotlib.pyplot as plt
import math

def fit_RANSAC(x_s,y_s,num_iterations=20,dist_allowed=2):
    points_matrix = np.hstack((x_s[:,np.newaxis],y_s[:,np.newaxis],np.ones(len(x_s))[:,np.newaxis]))
    best_params = []
    best_num_points = -1
    
    for it_idx in range(num_iterations):
        # select enough random points to build a line model (2) 
        rand.shuffle(points_matrix)
        
        # a = (y2-y1) / (x2-x1)
        # b = y2 - a * x2
        a = (points_matrix[1,1] - points_matrix[0,1]) / (points_matrix[1,0] - points_matrix[0,0])
        b = points_matrix[1,1] - a * points_matrix[1,0]
        # params: a*x - y + b = 0
        line_params = [ a,-1,b ]
        
        # calculate distance between the model and the points
        distances = np.abs(np.dot(points_matrix,line_params)) / math.sqrt(a*a + 1)
        
        # grab the points that are close
        points_close = points_matrix[distances <= dist_allowed]
        
        #fit the model into points close
        A = np.vstack([points_close[:,0], np.ones(len(points_close[:,0]))]).T  
        # params in form y = a*x + b       
        lst_sq_params = linalg.lstsq(A, points_close[:,1])[0]
        
        line_params = [lst_sq_params[0],-1,lst_sq_params[1]]
        
        # calculate distance between the model and the points
        distances = np.abs(np.dot(points_matrix,line_params)) / math.sqrt(a*a + 1)
        
        # grab the points that are close
        points_close = points_matrix[distances <= dist_allowed]
        
        #fit the model into points close
        A = np.vstack([points_close[:,0], np.ones(len(points_close[:,0]))]).T  
        # params in form y = a*x + b       
        lst_sq_params = linalg.lstsq(A, points_close[:,1])[0]
        
        # check whether model is better than any model before?
        num_points_fitting = np.sum(distances <= dist_allowed)
        if num_points_fitting > best_num_points:
            best_num_points = num_points_fitting
            best_params = lst_sq_params
            
    return best_params 

# line parameters in the y = a * x + b form
line_params = [10,1]

# noise params:
sigma = 8
mean = 0

x_s = np.arange(-10,10)

y_s = x_s * line_params[0] + line_params[1]
noisy_ys = y_s + sigma * rand.randn(len(x_s)) + mean

# least square fit
A = np.vstack([x_s, np.ones(len(x_s))]).T
lst_sq_params = linalg.lstsq(A, noisy_ys)[0]

fitted_ys = x_s * lst_sq_params[0] + lst_sq_params[1]

plt.plot(x_s,y_s,'x:',label="normal")
plt.plot(x_s,noisy_ys,'r*',label="noisy points")
plt.plot(x_s,fitted_ys,'g',label="fitted least squares")

plt.legend(loc="best")
#plt.show()

# add two significant outliers
noisy_ys[1] = 20
noisy_ys[16] = -60

# least square fit
A = np.vstack([x_s, np.ones(len(x_s))]).T
lst_sq_params = linalg.lstsq(A, noisy_ys)[0]

fitted_ys = x_s * lst_sq_params[0] + lst_sq_params[1]

# RANSAC fit
ransac_params = fit_RANSAC(x_s,noisy_ys)
ransac_ys = x_s * ransac_params[0] + ransac_params[1]

plt.figure()
plt.plot(x_s,y_s,'x:',label="normal")
plt.plot(x_s,noisy_ys,'r*',label="noisy points")
plt.plot(x_s,fitted_ys,'g',label="fitted least squares")
plt.plot(x_s,ransac_ys,'k',label="fitted RANSAC")

plt.legend(loc="best")
plt.show()




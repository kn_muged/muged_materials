import numpy as np
import numpy.random as rand
import matplotlib.pyplot as plt

import cv2
import cv2.cv as cv

filter_size = 5

image = cv2.imread("im2.ppm")
image_grey = cv2.cvtColor(image, cv.CV_BGR2GRAY)

sigma = 25
mean = 15
gaussian_noise = sigma * rand.randn(image.shape[0],image.shape[1]) + mean

image_grey_salt_pepper = np.where(gaussian_noise > 1*mean + 2.5*sigma,np.ones(gaussian_noise.shape)*255,image_grey)
image_grey_salt_pepper = np.array(image_grey_salt_pepper,dtype=np.ubyte)

noisy_image = image_grey + gaussian_noise

# filter using the box filter, gaussian filter
box_filtered = cv2.boxFilter(noisy_image, -1, (filter_size,filter_size))
gaussian_filtered = cv2.GaussianBlur(noisy_image, (filter_size,filter_size),0)

median_filtered = cv2.medianBlur(image_grey_salt_pepper, 3)
gaussian_filtered_salt = cv2.GaussianBlur(image_grey_salt_pepper, (3,3),0)

# sigma_colour = 10
# sigma_space = 15
# bilat_filtered = cv2.bilateralFilter(image, -1, sigma_colour, sigma_space)

plt.imshow(image_grey,cmap="gray")
plt.title("Grey scale image")

plt.figure()
plt.imshow(noisy_image,cmap="gray")
plt.title("Noisy image")

plt.figure()
plt.imshow(box_filtered,cmap="gray")
plt.title("Noisy image - box filtered")

plt.figure()
plt.imshow(gaussian_filtered,cmap="gray")
plt.title("Noisy image - gaussian filtered")

plt.figure()
plt.imshow(image_grey_salt_pepper,cmap="gray")
plt.title("Noisy (salt and pepper) image")

plt.figure()
plt.imshow(median_filtered,cmap="gray")
plt.title("Noisy (salt and pepper) image - median filtered")

plt.figure()
plt.imshow(gaussian_filtered_salt,cmap="gray")
plt.title("Noisy (salt and pepper) image - gaussian filtered")

# plt.figure()
# plt.imshow(bilat_filtered)
# plt.title("Bilateral filter")

plt.show()


import numpy as np
import numpy.random as rand
import matplotlib.pyplot as plt

import cv2
import cv2.cv as cv

image = cv2.imread("im2.ppm")

# here we can give any detector name supported by Opencv
detector = cv2.FeatureDetector_create("SIFT")

# or we can create detector directly - this allows us to pass extra
# arguments for the detector
detector = cv2.BRISK()

keypoints = detector.detect(image)

# extracting the descriptors
extractor = cv2.DescriptorExtractor_create("SIFT")
descriptors = extractor.compute(image,keypoints)
print descriptors

# descriptor matcher
#matcher = cv2.DescriptorMatcher_create(descriptorMatcherType)
#matcher.match(descriptors1,descriptors2)

image_with_keypoints = cv2.drawKeypoints(image, keypoints)

plt.imshow(image_with_keypoints)
plt.show()